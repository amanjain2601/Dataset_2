let data2 = require("./data2.js");

data2.reduce((previous, current) => {
    let splitIPAddress = current.ip_address.split(".");

    let splittedAddress = splitIPAddress.reduce((previousString, current, index, array) => {

        if (index < array.length - 1)
            previousString = previousString + current + " ";
        else
            previousString = previousString + current;

        return previousString;
    }, "")

    current.ip_address = splittedAddress;
})


console.log(data2);