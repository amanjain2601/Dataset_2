let data2 = require("./data2.js");


data2.sort(function (a, b) {
    let nameA = a.first_name.toUpperCase();
    let nameB = b.first_name.toUpperCase();

    if (nameA > nameB)
        return -1;

    if (nameA < nameB)
        return 1;

    return 0;
})

console.log(data2);