let data2 = require("./data2.js");


let sumOf2Components = data2.reduce((sumOf2Components, currentObject) => {
    let splittedAddress = currentObject.ip_address.split(".");
    sumOf2Components += parseInt(splittedAddress[1]);

    return sumOf2Components
}, 0)

console.log("Sum of 2 components", sumOf2Components);