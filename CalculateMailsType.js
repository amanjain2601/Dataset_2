let data2 = require("./data2.js")

let MailsTypesCount = data2.reduce((previous, currentObject) => {

    let splitByDot=currentObject.email.split(".");

    if (splitByDot[splitByDot.length-1]=="org") {
        if (previous["org"] == undefined)
            previous["org"] = 1;
        else
            previous["org"] += 1;
    }

    if (splitByDot[splitByDot.length-1]=="au") {
        if (previous["au"] == undefined)
            previous["au"] = 1;
        else
            previous["au"] += 1;
    }

    if (splitByDot[splitByDot.length-1]=="com") {
        if (previous["com"] == undefined)
            previous["com"] = 1;
        else
            previous["com"] += 1;
    }

    return previous;

}, {})

console.log(MailsTypesCount);