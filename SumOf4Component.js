let data2 = require("./data2.js");


let sumOf4Components = data2.reduce((sumOf4Components, currentObject) => {
    let splittedAddress = currentObject.ip_address.split(".");
    sumOf4Components += parseInt(splittedAddress[3]);

    return sumOf4Components;
}, 0)

console.log("Sum of 4 components", sumOf4Components);